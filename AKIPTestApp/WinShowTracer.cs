﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using AKIP.Classes;

namespace AKIPTestApp
{
    public class LogTracer : ITracer
    {
        private int _maxLength = -1;

        public LogTracer()
        {
            TraceLog = new ObservableCollection<string>();
        }

        public int MaxLength
        {
            get { return _maxLength; }
            set { _maxLength = value; }
        }

        public ObservableCollection<string> TraceLog { get; protected set; } 

        public void Trace(string msg)
        {
            TraceLog.Add(msg);

            //контроль максимальной длины лога 
            if(_maxLength>0)
                while (TraceLog.Count > _maxLength)
                    TraceLog.RemoveAt(0);
        }
    }
}
