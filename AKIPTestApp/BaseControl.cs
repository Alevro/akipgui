﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using AKIP.Classes;

namespace AKIPTestApp
{
    public class BaseControl : UserControl
    {
        protected AkipDevice _akip;
        public BaseControl(AkipDevice akip)
        {
            _akip = akip;
        }
    }
}
