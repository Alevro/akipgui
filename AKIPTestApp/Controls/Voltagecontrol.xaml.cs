﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AKIP.Classes;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для Voltagecontrol.xaml
    /// </summary>
    public partial class Voltagecontrol : UserControl
    {
        private AkipDevice _akip;
        public Voltagecontrol(AkipDevice akip)
        {
            _akip = akip;
            InitializeComponent();
            InsertControls();
        }
        public enum meth
        {
            Start,
            GetCHA,   //freqency, amplitude
            GetCHB,    //freqency, amplitude, offset
            GetHigh,     //freqency, amplitude
            GetLow,      //freqency, amplitude, offset
            GetOffset,        //freqency, amplitude, offset
            SetCHA,       //freqency, amplitude, offset
            SetCHB,         //freqency, amplitude, offset
            SetHigh,        //freqency, amplitude, offset
            SetLow,       //amplitude,offset
            SetOffset,          //offset
            END
        };
        public enum label
        {
            Amplitude = 6,
            AmplitudE = 7,
            Voltage = 8,
            VoltagE = 9,
            Offset = 10,

            END
        };
        private void InsertControls()
        {
            label lab = new label();
            Mathe d = new Mathe();
            string[] izm = new string[] { "Vpp", "Vpp", "Vdc", "Vdc", "Vdc", "Vpp", "Vpp", "Vdc", "Vdc", "Vdc" };
            for (var i = meth.Start + 1; i < meth.END; ++i)
            {
                lab++;
                var el = new DropList(_akip, d.Doit(this.ToString(), i.ToString()));
                el.Izm.Content = izm[(int)i-1];
                el.Method.Content = i.ToString();
                if (lab.ToString().Length > 3) el.Arg.Content = lab.ToString().ToUpper(); else { el.Arg.Content = ""; el.Drop.Visibility = Visibility.Hidden; }
                StackPnl.Children.Add(el);

            }
        }
    }
}
