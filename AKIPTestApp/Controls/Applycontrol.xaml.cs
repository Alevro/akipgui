﻿using AKIP.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для Applycontrol.xaml
    /// </summary>
    public partial class Applycontrol : UserControl
    {

        private AkipDevice apply;
        public Applycontrol(AkipDevice _apply)
        {
            apply = _apply;
            InitializeComponent();
            InsertControls();
        }
        public enum meth
        {
            Start,
            SetSinusoidA,   //freqency, amplitude
            SetSinusoid,    //freqency, amplitude, offset
            SetSquareA,     //freqency, amplitude
            SetSquare,      //freqency, amplitude, offset
            SetRamp,        //freqency, amplitude, offset
            SetPulse,       //freqency, amplitude, offset
            SetExp,         //freqency, amplitude, offset
            SetSinc,        //freqency, amplitude, offset
            SetNoise,       //amplitude,offset
            SetDC,          //offset
            GetCHA,         //no args
            GetCHB,         //no args
            END
        };

        private void InsertControls()
        {


            for (var i = meth.Start + 1; i < meth.END; ++i)
            {
                var el = new ApplyArg(apply);
                el.Method.Content = i.ToString();
                el.Arg2.IsEnabled = false;
                el.Arg3.IsEnabled = false;
                if (i == meth.GetCHA || i == meth.GetCHB)
                {
                    el.Arg1.Visibility = Visibility.Hidden;
                    el.Arg1l.Visibility = Visibility.Hidden;
                    el.Arg2.Visibility = Visibility.Hidden;
                    el.Arg2l.Visibility = Visibility.Hidden;
                    el.Arg3.Visibility = Visibility.Hidden;
                    el.Arg3l.Visibility = Visibility.Hidden;
                }
                StackPnl.Children.Add(el);
            }
        }
    }
}
