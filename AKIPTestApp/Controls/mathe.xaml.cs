﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class Mathe : UserControl
    {
        public string Doit(string name, string method)
        {
            string[] tmp = name.Split('.');
            string control = tmp[2];

            switch (control)
            {
                case "Amcontrol":
                    switch (method)
                    {
                        case "SetDepth": return "0,-10,-11";
                        case "SetInternalFrequency": return "0,-10,-11";
                        case "SetInternalFunction": return "-1,-2,-3";
                        case "SetSource": return "-16,-17";
                        default: return string.Empty;
                    }
                case "Burstcontrol":
                    switch (method)
                    {
                        case "SetNCycles": return "0,-10,-11";
                        case "SetPeriod":  return "0,-10,-11";
                        case "SetPhase":  return "0,-10,-11";
                        case "SetSource": return "-9,-16,-17";
                        default: return string.Empty;
                    }
                case "Fmcontrol":
                    switch (method)
                    {
                        case "SetDeviation": return "0,-10,-11";
                        case "SetInternalFrequency": return "0,-10,-11";
                        case "SetInternalFunction": return "0,-10,-11";
                        case "SetSource":  return "-16,-17";
                        default: return string.Empty;
                    }
                case "FreqControl":
                    switch (method)
                    {
                        case "GetCHA": return "0,-10,-11";
                        case "GetCHB": return "0,-10,-11";
                        case "SetCHA": return "0,-10,-11";
                        case "SetCHB": return "0,-10,-11";
                        case "SetStart": return "0,-10,-11";
                        case "SetStep":  return "0,-10,-11";
                        case "SetStop": return "0,-10,-11";
                        default: return string.Empty;
                    }
                case "Fskeycontrol":
                    switch (method)
                    {
                        case "SetHoppingFrequency": return "0,-10,-11";
                        case "SetShiftFrequency": return "0,-10,-11";
                        case "SetModulatingSource": return "-16,-17";
                        default: return string.Empty;
                    }
                case "Funccontrol":
                    switch (method)
                    {

                        case "SetCHA": return "-1,-2";
                        case "SetCHB": return "-1,-2,-3,-4,-5,-6,-7,-8";
                        case "SetPulsePeriod": return "0,-10,-11";
                        case "SetPulseWidth": return "0,-10,-11";
                        case "SetRampSymmetry": return "0,-10,-11";
                        case "SetSquareDcycle": return "0,-10,-11";
                        default: return string.Empty;
                    }
                case "Outputcontrol":
                    switch (method)
                    {
                        case "SetLoad": return "-14,-15";
                        case "SetLoadb": return "-14,-15";
                        case "SetTTL": return "-12,-13";
                        case "SetCHA": return "-12,-13";
                        case "SetCHB": return "-12,-13";
                        default: return string.Empty;
                    }
                case "Pskeycontrol":
                    switch (method)
                    {
                        case "SetPhase1": return "0,-10,-11";
                        case "SetPhase2": return "0,-10,-11";
                        case "SetRate": return "0,-10,-11";
                        case "SetSource": return "-16,-17";
                        default: return string.Empty;
                    }
                case "Sweepcontrol":
                    switch (method)
                    {
                        case "SetSource": return "-16,-17,-9";
                        case "SetSPACing": return "-18,-19";
                        case "SetTIME": return "0,-10,-11";
                        default: return string.Empty;
                    }
                case "Sysconf":
                    switch (method)
                    {
                        case "SetAutoMemoryRecall": return "-12,-13";
                        case "SetBeeper": return "-12,-13";
                        case "SetDisplay": return "-12,-13";
                        case "SetRCL": return "0";
                        case "SetSAV": return "0";
                        default: return string.Empty;
                    }
                case "triggercontrol":
                    switch (method)
                    {
                        case "SetSLOPe": return "-20,-21";
                        default: return string.Empty;
                    }
                case "Voltagecontrol":
                    switch (method)
                    {
                        case "SetCHA": return "0,-10,-11";
                        case "SetCHB": return "0,-10,-11";
                        case "SetHigh": return "0,-10,-11";
                        case "SetLow": return "0,-10,-11";
                        case "SetOffset": return "0,-10,-11";
                        default: return string.Empty;
                    }



                default:
                    break;
            }

            return "";


        }


    }
}
