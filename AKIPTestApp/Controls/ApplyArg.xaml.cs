﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AKIP.Classes;
using AKIP;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для ApplyArg.xaml
    /// </summary>
    public partial class ApplyArg : UserControl
    {
        public enum Freq_arg
        {
            NULL = 0,
            SINusoid = -1,
            SQUare = -2,
            RAMP = -3,
            PULSe = -4,
            EXP = -5,
            SINC = -6,
            NOISe = -7,
            DC = -8,
            BUS = -9,
            MINimum = -10,
            MAXimum = -11,
            OFF = -12,
            ON = -13,
            LOW = -14,
            HIGH = -15,
            INTernal = -16,
            EXTernal = -17,
            LINear = -18,
            LOGarithmic = -19,
            NEGative = -20,
            POSitive = -21,
            GATed = -22,
        };
        private AkipDevice _akip;
        public ApplyArg(AkipDevice akip)
        {
            _akip = akip;
            InitializeComponent();
            var TB = new TextBox();
            var TB2 = new TextBox();
            var TB3 = new TextBox();
            TB.Width = 80;
            TB2.Width = 80;
            TB3.Width = 80;
            Arg1.Items.Add(TB);
            Arg2.Items.Add(TB2);
            Arg3.Items.Add(TB3);

            for (var i = Freq_arg.NULL; i > Freq_arg.GATed - 1; i--)
            {
                if (i == 0 || i == Freq_arg.MAXimum  || i == Freq_arg.MINimum)  {
                    Arg1.Items.Add(i.ToString());
                    Arg2.Items.Add(i.ToString());
                    Arg3.Items.Add(i.ToString());
                }
            }

        }

        void Method_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Arg1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Arg1.SelectedItem != null)
            {
                Arg2.IsEnabled = true;
                if (Arg1.SelectedItem.ToString() == "NULL") { Arg1.SelectedValue = null; Arg2.IsEnabled = false; Arg2.SelectedValue = null; Arg3.IsEnabled = false; Arg3.SelectedValue = null; }
            }
        }

        private void Arg2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Arg2.SelectedItem != null)
            {
                Arg3.IsEnabled = true;
                if (Arg2.SelectedItem.ToString() == "NULL") { Arg2.SelectedValue = null; Arg3.IsEnabled = false; Arg3.SelectedValue = null; }
            }
        }


        private void Method_Click_1(object sender, RoutedEventArgs e)
        {
            string mess1 = ""; string mess2 = ""; string mess3 = "";
            string txt;
            if (Arg1.Text == "")
            {
                object txtbox = Arg1.SelectedValue;
                TextBox TB1 = new TextBox();
                TB1 = (TextBox)txtbox;
                if (TB1 != null) mess1 = TB1.Text;
            }
            else mess1 = Arg1.Text;

            if (Arg2.Text == "")
            {
                object txtbox = Arg2.SelectedValue;
                TextBox TB2 = new TextBox();
                TB2 = (TextBox)txtbox;
                if (TB2 != null) mess2 = TB2.Text;
            }
            else mess2 = Arg2.Text;

            if (Arg3.Text == "")
            {
                object txtbox = Arg3.SelectedValue;
                TextBox TB3 = new TextBox();
                TB3 = (TextBox)txtbox;
                if (TB3 != null) mess3 = TB3.Text;
            }
            else mess3 = Arg3.Text;
            txt = "error";
            txt = execute(Method.Content.ToString(), mess1, mess2, mess3);

            @return.Text = txt;
        }

      private double GetDouble(string value)
      {
        if (string.IsNullOrEmpty(value)) return 0;
        double result = 0;
        if (!double.TryParse(value, NumberStyles.Any, new CultureInfo("en"), out result))
        {
          if(!double.TryParse(value, NumberStyles.Any, new CultureInfo("ru-Ru"), out result))
          {
            return 0;
          }
        }
        return result;
      }

      private string execute(string method, string arg1 = "", string arg2 = "", string arg3 = "")
        {
            var apply = typeof(Apply);
            object[] oArg = null;
            var f = 0;

            for (var i = Freq_arg.SINusoid; i > Freq_arg.GATed - 1; i--)
            {
                f--;
                if (arg1 == i.ToString()) arg1 = f.ToString();
                if (arg2 == i.ToString()) arg2 = f.ToString();
                if (arg3 == i.ToString()) arg3 = f.ToString();
            }
            oArg = new object[] { GetDouble(arg1), GetDouble(arg2), GetDouble(arg3) };
            
            ////////

            switch (method)
            {
              case "SetSinusoidA": return _akip.Apply.SetSinusoidA( oArg[0], oArg[1],  oArg[2]).ToString();
              case "SetSinusoid": return _akip.Apply.SetSinusoid( oArg[0], oArg[1],  oArg[2]).ToString();
              case "SetSquareA": return _akip.Apply.SetSquareA(oArg[0], oArg[1], oArg[2]).ToString();
              case "SetSquare": return _akip.Apply.SetSquare(oArg[0], oArg[1], oArg[2]).ToString();
              case "SetRamp": return _akip.Apply.SetRamp(oArg[0], oArg[1], oArg[2]).ToString();
              case "SetPulse": return _akip.Apply.SetPulse(oArg[0], oArg[1], oArg[2]).ToString();
              case "SetExp": return _akip.Apply.SetExp(oArg[0], oArg[1], oArg[2]).ToString();
              case "SetSinc": return _akip.Apply.SetSinc(oArg[0], oArg[1], oArg[2]).ToString();
              case "SetNoise": return _akip.Apply.SetNoise(oArg[0], oArg[1], oArg[2]).ToString();
              case "SetDC": return _akip.Apply.SetDC(oArg[0], oArg[1], oArg[2]).ToString();
              case "GetCHA": return _akip.Apply.GetCHA().ToString();
              case "GetCHB": return _akip.Apply.GetCHB().ToString();
              default: throw new Exception("непредвиденая ошибка");
            }

        }

    }
}



