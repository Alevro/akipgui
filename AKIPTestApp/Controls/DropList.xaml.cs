﻿using System.Globalization;
using AKIP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AKIP.Classes;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для DropList.xaml
    /// </summary>
    public partial class DropList : UserControl
    {
        public enum Freq_arg
        {
            SINusoid = -1,
            SQUare = -2,
            RAMP = -3,
            PULSe = -4,
            EXP = -5,
            SINC = -6,
            NOISe = -7,
            DC = -8,
            BUS = -9,
            MINimum = -10,
            MAXimum = -11,
            OFF = -12,
            ON = -13,
            LOW = -14,
            HIGH = -15,
            INTernal = -16,
            EXTernal = -17,
            LINear = -18,
            LOGarithmic = -19,
            NEGative = -20,
            POSitive = -21,
            GATed = -22,
        };

        private AkipDevice _akip;
        Freq_arg list;
        public DropList(AkipDevice akip, string coms="")
        {
            string[] mas = new string[] { };
            if (coms != "") {mas = coms.Split(','); } 

            list = new Freq_arg();
            _akip = akip;
            InitializeComponent();
            var TB = new TextBox();
            TB.Width = 80;
            if (coms != "")
            {
                if (mas[0] == "0") 
                {
                    Drop.Items.Add(TB);
                    for (var i = 1; i < mas.Length; i++)
                    {
                        Drop.Items.Add((Freq_arg)Convert.ToInt16(mas[i]));
                    }
                    Drop.SelectedIndex = 0;
                }
                else
                {
                    for (var i = 0; i < mas.Length; i++)
                    {
                        Drop.Items.Add((Freq_arg)Convert.ToInt16(mas[i]));
                    }
                    Drop.SelectedIndex = 0;
                }
            }
        }

        private void Method_Click(object sender, RoutedEventArgs e)
        {
            string nameGB;
            StackPanel sp;
            sp = (StackPanel)Method.Parent;
            GroupBox gb;
            DropList dl;
            Control c;
            dl = (DropList)sp.Parent;
            sp = (StackPanel)dl.Parent;
            c = (Control)sp.Parent;
            gb = (GroupBox)c.Parent;
            nameGB = gb.Header.ToString();

            var mess="";
            if (Drop.Text == "")
            {
              var TB = (TextBox)Drop.SelectedValue;
              if (TB != null)
              {
                mess = TB.Text;
              }
            }
            else
            {
              mess = Drop.Text;
            }
            var txt = Execute(Method.Content.ToString(), mess, nameGB);
            @return.Text = txt;
        }

        private string Execute(string method, string Arg, string cntrl)
        {
            var am = typeof(AM);
            var fm = typeof(FM);
            var burst = typeof(Burst);
            var frequency = typeof(Frequency);
            var fskey = typeof(FSkey);
            var function = typeof(Function);
            var ouput = typeof(Output);
            var pskey = typeof(PSkey);
            var sweep = typeof(Sweep);
            var sysconf = typeof(SysConf);
            var trigger = typeof(AKIP.Trigger);
            var voltage = typeof(Voltage);
            var apply = typeof(Apply);

            double oArg=0;
            int f = 0;
            if (Arg != "")
            {
                for (var i = Freq_arg.SINusoid; i > Freq_arg.GATed - 1; i--)
                {
                    f--;
                  if (Arg != i.ToString()) continue;
                  Arg = f.ToString();break ;
                }
              if (!double.TryParse(Arg, NumberStyles.Float, new CultureInfo("en"), out oArg))
              {
                if (!double.TryParse(Arg, NumberStyles.Float, new CultureInfo("ru-Ru"), out oArg))
                {
                }
              }
            }

            switch (cntrl)
            {


                ////////
                case "AM":

                    switch (method)
                    {
                        case "GetDepth": return _akip.AM.GetDepth().ToString();
                        case  "GetInternalFrequency":return _akip.AM.GetInternalFrequency().ToString();
                        case "GetInternalFunction":return _akip.AM.GetInternalFunction().ToString();
                        case "GetSource":return _akip.AM.GetSource().ToString();
                        case "SetDepth":return _akip.AM.SetDepth(oArg).ToString();
                        case "SetInternalFrequency":return _akip.AM.SetInternalFrequency(oArg).ToString();
                        case "SetInternalFunction": return _akip.AM.SetInternalFunction((IntFunct)oArg).ToString();
                        case "SetSource":return _akip.AM.SetSource((Source)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }
                    
                ////////
                case "FM":
                    switch (method)
                    {
                        case "GetDeviation": return _akip.FM.GetDeviation().ToString();
                        case "GetInternalFrequency": return _akip.FM.GetInternalFrequency().ToString();
                        case "GetInternalFunction": return _akip.FM.GetInternalFunction().ToString();
                        case "GetSource": return _akip.FM.GetSource().ToString();
                        case "SetDeviation": return _akip.FM.SetDeviation(oArg).ToString();
                        case "SetInternalFrequency": return _akip.FM.SetInternalFrequency(oArg).ToString();
                        case "SetInternalFunction": return _akip.FM.SetInternalFunction((IntFunct)oArg).ToString();
                        case "SetSource": return _akip.FM.SetSource((Source)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }
                ////////
                case "BURST":
                    switch (method)
                    {
                        case "GetPeriod": return _akip.Burst.GetPeriod().ToString();
                        case "GetPhase": return _akip.Burst.GetPhase().ToString();
                        case "GetSource": return _akip.Burst.GetSource().ToString();
                        case "SetNCycles": return _akip.Burst.SetNCycles(oArg).ToString();
                        case "SetPeriod": return _akip.Burst.SetPeriod(oArg).ToString();
                        case "SetPhase": return _akip.Burst.SetPhase(oArg).ToString();
                        case "SetSource": return _akip.Burst.SetSource((BurstSource)oArg).ToString();
                        case "GetNCycles": return _akip.Burst.GetNCycles().ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }

                ////////
                case "FREQUENCY":
                    switch (method)
                    {
                        case "GetCHA": return _akip.Frequency.GetCHA((MinMax)oArg).ToString();
                        case "GetCHB": return _akip.Frequency.GetCHB((MinMax)oArg).ToString();
                        case "GetStart": return _akip.Frequency.GetStart().ToString();
                        case "GetStep": return _akip.Frequency.GetStep().ToString();
                        case "GetStop": return _akip.Frequency.GetStop().ToString();
                        case "SetCHA": return _akip.Frequency.SetCHA((MinMax)oArg).ToString();
                        case "SetCHB": return _akip.Frequency.SetCHB((MinMax)oArg).ToString();
                        case "SetStart": return _akip.Frequency.SetStart((MinMax)oArg).ToString();
                        case "SetStep": return _akip.Frequency.SetStep((MinMax)oArg).ToString();
                        case "SetStop": return _akip.Frequency.SetStop((MinMax)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }
                ////////
                case "FSKEY":
                    switch (method)
                    {
                        case "GetHoppingFrequency": return _akip.FSkey.GetHoppingFrequency().ToString();
                        case "GetShiftFrequency": return _akip.FSkey.GetShiftFrequency().ToString();
                        case "GetModulatingSource": return _akip.FSkey.GetModulatingSource().ToString();
                        case "SetHoppingFrequency": return _akip.FSkey.SetHoppingFrequency((MinMax)oArg).ToString();
                        case "SetShiftFrequency": return _akip.FSkey.SetShiftFrequency((MinMax)oArg).ToString();
                        case "SetModulatingSource": return _akip.FSkey.SetModulatingSource((Source)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }
                ////////
                case "FUNCTION":
                    switch (method)
                    {
                        case "GetCHA": return _akip.Function.GetCHA().ToString();
                        case "GetCHB": return _akip.Function.GetCHB().ToString();
                        case "GetPulsePeriod": return _akip.Function.GetPulsePeriod().ToString();
                        case "GetPulseWidth": return _akip.Function.GetPulseWidth().ToString();
                        case "GetRampSymmetry": return _akip.Function.GetRampSymmetry((MinMax)oArg).ToString();
                        case "GetSquareDcycle": return _akip.Function.GetSquareDcycle().ToString();
                        case "SetCHA": return _akip.Function.SetCHA((FuncCHA) oArg).ToString();
                        case "SetCHB": return _akip.Function.SetCHB((FuncCHB)oArg).ToString();
                        case "SetPulsePeriod": return _akip.Function.SetPulsePeriod((MinMax)oArg).ToString();
                        case "SetPulseWidth": return _akip.Function.SetPulseWidth((MinMax)oArg).ToString();
                        case "SetRampSymmetry": return _akip.Function.SetRampSymmetry((MinMax)oArg).ToString();
                        case "SetSquareDcycle": return _akip.Function.SetSquareDcycle((MinMax)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }
                ////////
                case "OUTPUT":
                    switch (method)
                    {
                        case "GetLoad": return _akip.Output.GetLoad().ToString();
                        case "GetLoadb": return _akip.Output.GetLoadb().ToString();
                        case "GetCHA": return _akip.Output.GetCHA().ToString();
                        case "GetTTL": return _akip.Output.GetTTL().ToString();
                        case "SetLoad": return _akip.Output.SetLoad((HiLo)oArg).ToString();
                        case "SetLoadb": return _akip.Output.SetLoadb((HiLo)oArg).ToString();
                        case "SetTTL": return _akip.Output.SetTTL((OnOff)oArg).ToString();
                        case "GetCHB": return _akip.Output.GetCHB().ToString();
                        case "SetCHA": return _akip.Output.SetCHA((OnOff)oArg).ToString();
                        case "SetCHB": return _akip.Output.SetCHB((OnOff)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }
                ////////
                case "PSKEY":
                    switch (method)
                    {
                        case "GetPhase1": return _akip.PSkey.GetPhase1().ToString();
                        case "GetPhase2": return _akip.PSkey.GetPhase2().ToString();
                        case "GetRate": return _akip.PSkey.GetRate().ToString();
                        case "SetPhase1": return _akip.PSkey.SetPhase1((MinMax)oArg).ToString();
                        case "SetPhase2": return _akip.PSkey.SetPhase2((MinMax)oArg).ToString();
                        case "SetRate": return _akip.PSkey.SetRate((MinMax)oArg).ToString();
                        case "SetSource": return _akip.PSkey.SetSource((Source)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }

                ////////
                case "SWEEP":
                    switch (method)
                    {
                        case "GetSource": return _akip.Sweep.GetSource().ToString();
                        case "GetSPACing": return _akip.Sweep.GetSPACing().ToString();
                        case "GetTIME": return _akip.Sweep.GetTIME().ToString();
                        case "SetSource": return _akip.Sweep.SetSource((SourceB)oArg).ToString();
                        case "SetSPACing": return _akip.Sweep.SetSPACing((LinLog)oArg).ToString();
                        case "SetTIME": return _akip.Sweep.SetTIME((MinMax)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }

                ////////
                case "SYSCONF":
                    switch (method)
                    {
                        case "GetAutoMemoryRecall": return _akip.SysConf.GetAutoMemoryRecall().ToString();
                        case "GetBeeper": return _akip.SysConf.GetBeeper().ToString();
                        case "GetDisplay": return _akip.SysConf.GetDisplay().ToString();
                        case "GetVersion": return _akip.SysConf.GetVersion().ToString();
                        case "SetAutoMemoryRecall": return _akip.SysConf.SetAutoMemoryRecall((OnOff)oArg).ToString();
                        case "SetBeeper": return _akip.SysConf.SetBeeper((OnOff)oArg).ToString();
                        case "SetDisplay": return _akip.SysConf.SetDisplay((OnOff)oArg).ToString();
                        case "SetLocal": return _akip.SysConf.SetLocal().ToString();
                        case "SetRCL": return _akip.SysConf.SetRCL((int)oArg).ToString();
                        case "SetReset": return _akip.SysConf.SetReset().ToString();
                        case "SetSAV": return _akip.SysConf.SetSAV((int)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }
                ////////
                case "TRIGGER":
                    switch (method)
                    {
                        case "GetSLOPe": return _akip.Trigger.GetSLOPe().ToString();
                        case "SetSLOPe": return _akip.Trigger.SetSLOPe((Polar)oArg).ToString();
                        case "SetTrigger": return _akip.Trigger.SetTrigger().ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }

                    ////////         
                case "VOLTAGE":
                    switch (method)
                    {
                        case "GetCHA": return _akip.Voltage.GetCHA().ToString();
                        case "GetCHB": return _akip.Voltage.GetCHB().ToString();
                        case "GetHigh": return _akip.Voltage.GetHigh().ToString();
                        case "GetLow": return _akip.Voltage.GetLow().ToString();
                        case "GetOffset": return _akip.Voltage.GetOffset().ToString();
                        case "SetCHA": return _akip.Voltage.SetCHA((MinMax)oArg).ToString();
                        case "SetCHB": return _akip.Voltage.SetCHB((MinMax)oArg).ToString();
                        case "SetHigh": return _akip.Voltage.SetHigh((MinMax)oArg).ToString();
                        case "SetLow": return _akip.Voltage.SetLow((MinMax)oArg).ToString();
                        case "SetOffset": return _akip.Voltage.SetOffset((MinMax)oArg).ToString();
                        default: throw new Exception("непредвиденая ошибка");
                    }
                default: return "";
            }
        }

    }



}

