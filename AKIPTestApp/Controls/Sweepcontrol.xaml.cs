﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AKIP.Classes;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для Sweepcontrol.xaml
    /// </summary>
    public partial class Sweepcontrol : UserControl
    {

        private AkipDevice _akip;
        public Sweepcontrol(AkipDevice akip)
        {
            _akip = akip;
            InitializeComponent();
            InsertControls();
        }
        public enum meth
        {
            Start,
            GetSource,
            GetSPACing,
            GetTIME,
            SetSource,
            SetSPACing,
            SetTIME,
            END
        };
        public enum label
        {
            source = 4,
            command = 5,
            second = 6,

            END
        };
        private void InsertControls()
        {
            label lab = new label();
            Mathe d = new Mathe();
            string[] izm = new string[] { "", "", "", "", "", "sec", "Hz", "Hz", "", "" };
            for (var i = meth.Start + 1; i < meth.END; ++i)
            {
                lab++;
                var el = new DropList(_akip, d.Doit(this.ToString(), i.ToString()));
                el.Izm.Content = izm[(int)i-1];
                el.Method.Content = i.ToString();
                if (lab.ToString().Length > 3) el.Arg.Content = lab.ToString().ToUpper(); else { el.Arg.Content = ""; el.Drop.Visibility = Visibility.Hidden; }
                StackPnl.Children.Add(el);

            }
        }
    }
}
