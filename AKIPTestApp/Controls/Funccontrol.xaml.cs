﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AKIP.Classes;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для Funccontrol.xaml
    /// </summary>
    public partial class Funccontrol : UserControl
    {
        private AkipDevice _akip;
        public Funccontrol(AkipDevice akip)
        {
            _akip = akip;
            InitializeComponent();
            InsertControls();
        }
        public enum meth
        {
            Start,
            GetCHA,
            GetCHB,
            GetPulsePeriod,
            GetPulseWidth,
            GetRampSymmetry,
            GetSquareDcycle,
            SetCHA,
            SetCHB,
            SetPulsePeriod,
            SetPulseWidth,
            SetRampSymmetry,
            SetSquareDcycle,
            END
        };
        public enum label
        {
            Command = 7,
            COmmand = 8,
            Second = 9,
            SEcond = 10,
            percentage = 11,
            Percentage = 12,

            END
        };
        private void InsertControls()
        {
            label lab = new label();
            Mathe d = new Mathe();
            string[] izm = new string[] { " ", " ", "sec", "%", "%", "%", "" , "", "sec", "%", "%", "" };
            for (var i = meth.Start + 1; i < meth.END; ++i)
            {
                lab++;
                var el = new DropList(_akip, d.Doit(this.ToString(), i.ToString()));
                el.Izm.Content = izm[(int)i-1];
                el.Method.Content = i.ToString();
                if (lab.ToString().Length > 3) el.Arg.Content = lab.ToString().ToUpper(); else { el.Arg.Content = ""; el.Drop.Visibility = Visibility.Hidden; }
                StackPnl.Children.Add(el);

            }
        }
    }
}
