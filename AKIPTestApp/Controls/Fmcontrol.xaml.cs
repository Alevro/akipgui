﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AKIP.Classes;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для Fmcontrol.xaml
    /// </summary>
    public partial class Fmcontrol : UserControl
    {
        private AkipDevice _akip;
        public Fmcontrol(AkipDevice akip)
        {
            _akip = akip;
            InitializeComponent();
            InsertControls();
        }
        public enum meth
        {
            Start,
            GetDeviation,
            GetInternalFrequency,
            GetInternalFunction,
            GetSource,
            SetDeviation,
            SetInternalFrequency,
            SetInternalFunction,
            SetSource,

            END
        };
        public enum label
        {
            Deviation = 5,   //freqency, amplitude
            Frequency = 6,    //freqency, amplitude, offset
            Function = 7,     //freqency, amplitude
            Source = 8,      //freqency, amplitude, offset

            END
        };
        private void InsertControls()
        {
            label lab = new label();
            Mathe d = new Mathe();
            string[] izm = new string[] { "Hz", "Hz", "", "", "Hz", "Hz", " ", " ", "", "" };
            for (var i = meth.Start + 1; i < meth.END; ++i)
            {
                lab++;
                var el = new DropList(_akip, d.Doit(this.ToString(), i.ToString()));
                el.Izm.Content = izm[(int)i-1];
                el.Method.Content = i.ToString();
                if (lab.ToString().Length > 3) el.Arg.Content = lab.ToString().ToUpper(); else { el.Arg.Content = ""; el.Drop.Visibility = Visibility.Hidden; }
                StackPnl.Children.Add(el);

            }
        }
    }
}
