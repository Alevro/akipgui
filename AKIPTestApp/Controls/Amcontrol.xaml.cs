﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AKIP.Classes;

namespace AKIPTestApp.Controls
{
    /// <summary>
    /// Логика взаимодействия для Amcontrol.xaml
    /// </summary>
    public partial class Amcontrol : UserControl
    {
        private AkipDevice _akip;
        public Amcontrol(AkipDevice akip)
        {
            _akip = akip;
            InitializeComponent();
            InsertControls();
        }

        public enum  meth 
        {
            Start,
            GetDepth,
            GetInternalFrequency,
            GetInternalFunction,
            GetSource,
            SetDepth,
            SetInternalFrequency,
            SetInternalFunction,
            SetSource,
            END
        };

        public enum label
        {
            
            Depth = 5,
            Frequency = 6,
            Source = 7,
            Function = 8,

            END
        };
        private void InsertControls()
        {
            Mathe d = new Mathe();
            label lab = new label();
            string[] izm = new string[] { "%", "Hz", "", "", "%", "Hz", "Hz", "Hz", "", "" };
            for (var i = meth.Start + 1; i < meth.END; ++i)
            {
                string result = d.Doit(this.ToString(), i.ToString());

                lab++;
                var el = new DropList(_akip,  d.Doit(this.ToString(), i.ToString()));
                el.Izm.Content = izm[(int)i-1];
                el.Method.Content = i.ToString();
                if (lab.ToString().Length > 3) el.Arg.Content = lab.ToString().ToUpper(); else { el.Arg.Content = ""; el.Drop.Visibility = Visibility.Hidden; }
                StackPnl.Children.Add(el);

            }
        }
    }
}
