﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AKIP;
using AKIP.Classes;
using AKIP.Interfaces;
using AKIPTestApp.Controls;

namespace AKIPTestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IAkipDevice akip;
        private ITracer _tracer;

        public MainWindow()
        {
            InitializeComponent();

            _tracer = new LogTracer();
            DataContext = _tracer;
            akip = new AkipDevice(_tracer); // TODO
            var Dl = new DropList((AkipDevice)akip);
            InsertControls();
        }


        private void InsertControls()
        {
            var gbox = new GroupBox() { Header = "APPLY" };
            var el0 = new Applycontrol((AkipDevice)akip);
            gbox.Content = el0;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "AM" };
            var el1 = new Amcontrol((AkipDevice)akip);
            gbox.Content = el1;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "BURST" };
            var el2 = new Burstcontrol((AkipDevice)akip);
            gbox.Content = el2;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "FM" };
            var el3 = new Fmcontrol((AkipDevice)akip);
            gbox.Content = el3;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "FREQUENCY" };
            var el4 = new FreqControl((AkipDevice)akip);
            gbox.Content = el4;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "FSKEY" };
            var el5 = new Fskeycontrol((AkipDevice)akip);
            gbox.Content = el5;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "FUNCTION" };
            var el6 = new Funccontrol((AkipDevice)akip);
            gbox.Content = el6;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "OUTPUT" };
            var el7 = new Outputcontrol((AkipDevice)akip);
            gbox.Content = el7;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "PSKEY" };
            var el8 = new Pskeycontrol((AkipDevice)akip);
            gbox.Content = el8;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "SWEEP" };
            var el9 = new Sweepcontrol((AkipDevice)akip);
            gbox.Content = el9;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "SYSCONF" };
            var el10 = new Sysconf((AkipDevice)akip);
            gbox.Content = el10;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "TRIGGER" };
            var el11 = new triggercontrol((AkipDevice)akip);
            gbox.Content = el11;
            StackPnl.Children.Add(gbox);

            gbox = new GroupBox() { Header = "VOLTAGE" };
            var el12 = new Voltagecontrol((AkipDevice)akip);
            gbox.Content = el12;
            StackPnl.Children.Add(gbox);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (((Session)akip.GetSession()).Open(0)) stat.Text = "Состояние: Открыт";
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ((Session)akip.GetSession()).Close(); stat.Text = "Состояние: Закрыт";
        }



    }
}
